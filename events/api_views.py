from django.http import JsonResponse
from.acls import get_photo, get_weather_data
from .models import Conference, Location
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import State

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            )
    else:
        content = json.loads(request.body)

        #get State object and put it in content dictionary
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class LocationDetailEncoder (ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation}

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]
    encoders = {
        "location": LocationListEncoder
    }

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):

    if request.method=="GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    elif request.method=="DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
            )
    else: #if you want to update location then method==PUT
        content = json.loads(request.body)

        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid conference name"},
            status=400,
            )

        #here is getting the weather coordinates API~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        weather=get_weather_data(content["city"], content["state"].abbreviation)
        content.update(weather)

        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            {"conference": conference, "weather": weather}, #do I need to add "weather" to the Conference models?
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_locations(request):

    if request.method == "GET":
        locations = Location.objects.all()

        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            )
    else: #for the POST method
        content = json.loads(request.body)

        #get State object and put it in content dictionary
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        #here is getting the photo API~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        photo=get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method=="GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
    )
    elif request.method=="DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
            )
    else: #if you want to update location then method==PUT
        content = json.loads(request.body)

        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
