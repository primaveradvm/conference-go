from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json, requests

def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    params = {
        "query": f"{city}, {state}",
        "per_page": "1",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)

    try:
        content = response.json()
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except(KeyError, IndexError, requests.exceptions.JSONDecodeError):
        return{"picture_url":None}

def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state}, USA",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": "1",
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params) #returns a JSON
    try:
        content = response.json()
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except(KeyError, IndexError, requests.exceptions.JSONDecodeError):
        return{"latitude/longitude":None}

      # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    # params = {
    #     "lat":lat,
    #     "lon": lon,
    #     "appid": OPEN_WEATHER_API_KEY,
    #     "units": "imperial",
    # }
    # url ="https://api.openweathermap.org/data/2.5/weather/"
    # response = requests.get(url, params=params) #.get returns JSON string
    # try:
    #     content = response.json() #converts JSON to Python (happens to be a dict here)

    #     description = content[“weather”][]
    #     temp = content{“main”}{“temp”}
